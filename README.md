# **_ MEMORY GAME _**

Partiendo de la base que tengo en programación 💻 ( **Html, CSS, JavaScript** ) he creado un pequeño juego de memoria, consiste en recordar donde estan los pares de cartas iguales y emparejarlas.

---

✅ Para ello he creado un `index.html` lo más correcto posible, con una estructura sencilla. He creado algunas etiquetas `<meta/>` que considero que tienen relativa importancia ( _author, description, keywords ..._ ).

✅ El siguiente paso fue ponerse con el archivo `.js` que será el encargado de realizar todo lo necesario para lo que queremos realizar en nuestro juego .

```
function reiniciarJuego() {
  //Limpio las variables necesarias para que el juego inicie de cero
  emojies = createArrayEmojies();
  intentos = 0;
  contadorAciertos = 0;
  turno = 0;
  previousCard = '';

```

✅ Y para finalizar, he realizado el diseño, gracias al documento `style.css` le doy una imagen visual a la creación, tratando de proporcionarle un aspecto interesante, que no dañe demasiado la vista 👀 .

![Pequeña Imagen de creación](https://media.tenor.com/blfxNAIWbpAAAAAC/hmm-thinking.gif)

---

> " La memoria es el único paraíso del que no podemos ser expulsados. " — Jean Paul

---

### **Autor ✒️ :**

Todo el trabajo ha sido creado por :

- <span style="color:green">Marcos</span> : [LinkedIn](https://es.linkedin.com/in/marcos-v%C3%A1zquez-gonz%C3%A1lez-44379562?trk=people-guest_people_search-card)
  ➡️ Creador de la estructura base en `html`, `javascript` y de diseñar la parte visual de `CSS`.

---


## Funcionalidades :

> - 100% Responsive en Inglés. </br>
> - Contador de movimientos. </br>
> - Botón de reseteo. </br>

---

## Web de la Aplicación ⚙ :

- [Link](https://stunning-semolina-341e22.netlify.app/)

---
